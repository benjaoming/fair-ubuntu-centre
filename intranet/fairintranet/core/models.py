from __future__ import unicode_literals
from __future__ import absolute_import

from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from wagtail.core.models import Page
from wagtail.core.fields import RichTextField
from wagtail.admin.edit_handlers import FieldPanel
from wagtail.images.edit_handlers import ImageChooserPanel

from wagtailmarkdown.fields import MarkdownField


COMMON_CONTENT_PANELS = [
    FieldPanel('title', classname="full title"),
    FieldPanel('description', classname="full"),
    FieldPanel('slug'),
    FieldPanel('show_in_menus',),
]

RESOURCE_PANELS = [
    ImageChooserPanel('thumbnail'),
    FieldPanel('resource_link'),
    FieldPanel('author'),
    FieldPanel('year'),
    FieldPanel('country'),
    FieldPanel('caption'),
]


class HomePage(Page):
    """
    Model for the main page, displaying the top layer of resources.
    """
    
    body = RichTextField(null=True, blank=True, verbose_name=_("Body / main text on page"), editable=False)
    description = MarkdownField(null=True, blank=True, verbose_name=_("Markdown description"))
    menu_order = models.PositiveSmallIntegerField(default=100)
    
    def child_collections(self):
        return Collection.objects.child_of(self).order_by('menu_order', 'title')
    
    class Meta:
        verbose_name = _("Standard article")
        verbose_name = _("Standard articles")


HomePage.content_panels = COMMON_CONTENT_PANELS

HomePage.promote_panels = [
    FieldPanel('seo_title',),
    FieldPanel('search_description',),
    FieldPanel('menu_order'),
]


class Resource(models.Model):
    """
    An abstract model for a concrete resource such as an ebook or movie.
    """
    
    caption = models.CharField(
        max_length=512,
        null=True,
        blank=True,
    )
    # This is not a URLField, because it doesn't validate non-domain
    # http://server/path/
    resource_link = models.CharField(
        verbose_name=_("resource link"),
        help_text=_("Find a book or movie file available on the intranet and copy the link here, e.g. http://localserver/path/to/book.pdf",),
        max_length=512,
    )
    thumbnail = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    year = models.PositiveSmallIntegerField(null=True, blank=True, verbose_name=_("year of publication"))
    author = models.CharField(max_length=64, null=True, blank=True, verbose_name=_("author / director"))
    country = models.CharField(max_length=64, null=True, blank=True, verbose_name=_("country of origin"))
    source_yaml_name = models.SlugField(editable=False, null=True, blank=True)
    license = models.CharField(max_length=64, null=True, blank=True, verbose_name=_("license"))

    class Meta:
        abstract = True


class EBook(HomePage, Resource):
    pass

EBook.content_panels = COMMON_CONTENT_PANELS + RESOURCE_PANELS


class Movie(HomePage, Resource):
    duration = models.CharField(max_length=64, null=True, blank=True)

Movie.content_panels = COMMON_CONTENT_PANELS + RESOURCE_PANELS + [FieldPanel('duration')]


class Collection(HomePage):
    """
    A collection displays resources: Either links to external collections,
    movies or ebooks.
    """
    
    icon = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+',
        verbose_name=_("icon"),
        help_text=_("Cheerful icon to display on buttons next to the title"),
    )
    icon_predefined = models.CharField(
        max_length=64,
        default="folder-open",
        verbose_name=_("default icon"),
        choices=[
            (x, x) for x in [
                'folder-open',
                'video-camera',
                'truck',
                'rocket',
                'road',
                'question',
                'graduation-cap',
                'globe',
                'money',
                'desktop',
                'microphone',
                'code',
                'book',
                'comment',
            ]
        ]
    )
    license = models.CharField(max_length=64, null=True, blank=True, verbose_name=_("license"))
    caption = models.CharField(max_length=128, null=True, blank=True, verbose_name=_("button caption"))
    source_yaml_name = models.SlugField(editable=False, null=True, blank=True)

    def get_movies(self):
        return Movie.objects.child_of(self).order_by("title")

    def get_ebooks(self):
        return EBook.objects.child_of(self).order_by("title")

    def get_external_collections(self):
        return ExternalCollection.objects.child_of(self).order_by("title")

    def get_non_external_collections(self):
        return Collection.objects.filter(externalcollection=None).child_of(self).order_by("title")


Collection.content_panels = COMMON_CONTENT_PANELS + [
    ImageChooserPanel('icon'),
    FieldPanel('icon_predefined'),
    FieldPanel('license'),
    FieldPanel('caption'),
]


class ExternalCollection(Collection):
    retrieved = models.DateField(null=True, blank=True, verbose_name=_("Retrieval date"), help_text=_("When this collection was copied from the internet"))

    # This is not a URLField, because it doesn't validate non-domain
    # http://server/path/
    resource_link = models.CharField(
        verbose_name=_("resource link"),
        help_text=_("Find a book or movie file available on the intranet and copy the link here, e.g. http://localserver/path/to/book.pdf",),
        max_length=512,
    )

ExternalCollection.content_panels = Collection.content_panels + [
    FieldPanel('retrieved'),
    FieldPanel('resource_link'),
]


class ResourceUsage(models.Model):
    """
    This model tracks clicks on a resource for statistical purposes
    """
    
    ebook = models.ForeignKey('EBook', null=True, blank=True, on_delete=models.CASCADE)
    movie = models.ForeignKey('Movie', null=True, blank=True, on_delete=models.CASCADE)
    external_collection = models.ForeignKey('ExternalCollection', null=True, blank=True, on_delete=models.CASCADE)
    clicks = models.PositiveIntegerField(default=0)
    from_date = models.DateField()
    to_date = models.DateField()
    
    class Meta:
        verbose_name = _("resource usage")
        verbose_name_plural = _("resource usages")
        ordering = ('from_date', 'to_date')
    
    @classmethod
    def count_click(cls, **kwargs):
        """"""
        if 'movie' not in kwargs and 'ebook' not in kwargs and 'external_collection' not in kwargs:
            raise RuntimeError("You must specify either movie or ebook or external_collection")
        from_date = timezone.now().replace(day=1).date()
        if from_date.month < 12:
            to_date = from_date.replace(month=from_date.month + 1)
        else:
            to_date = from_date.replace(month=1, year=from_date.year + 1)
        kwargs['from_date'] = from_date
        kwargs['to_date'] = to_date
        usage, _ = cls.objects.get_or_create(**kwargs)
        usage.clicks += 1
        usage.save()
        return usage
