from django.conf import settings
from django.contrib import admin
from django.urls import include
from django.urls import path, re_path
from django.conf.urls.static import static

from core import views as core_views


urlpatterns = [
    path('django-admin/', admin.site.urls),
    re_path(r'^download/movie/(\d+)/$', core_views.download_movie, name='download_movie'),
    re_path(r'^download/ebook/(\d+)/$', core_views.download_ebook, name='download_ebook'),
    re_path(r'^download/external-collection/(\d+)/$', core_views.download_external_collection, name='download_external_collection'),
    path('technicians/', include("technicians.urls", namespace='technicians')),
    path('admin/', include("wagtail.admin.urls")),
    path('documents/', include("wagtail.documents.urls")),

    path('', include("wagtail.core.urls")),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + \
static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
