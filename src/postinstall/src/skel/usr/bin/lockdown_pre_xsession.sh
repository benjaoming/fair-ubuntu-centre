#!/bin/bash

# Before installing
if test "$USER" = "student" || test "$USER" = "online"
then
	rm -rf ~/.cache/mozilla
        rm -Rf ~/.gconf
        rm -Rf ~/.gconfd

        rm -Rf ~/.config
        rm -Rf ~/.local
        rm -Rf ~/.gtk-bookmarks
        rm -Rf ~/.mozilla
        rm -Rf ~/.openoffice.org
        rm -Rf ~/.nautilus
        rm -Rf ~/.gstreamer-0.10

fi

