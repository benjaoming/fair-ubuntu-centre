echo "---------------------------------------"
echo "Copying TFTP root directory            "
echo "---------------------------------------"

echo "Copying tftp root directory... can take up to a couple of minutes..."

# Danger: DNSMasq has already been started, and counts on this TFTP directory being present (that's why the directory was created in network.sh)
# Remove it because we will unpack and write stuff to it, and in case that there
# are changes or updates, they have to be forcefully applied.
rm -Rf /var/tftp
mkdir /var/tftp

# The TFTP server is used to boot the clients from the network.  The DHCP server tells the clients to fetch their operating system over TFTP.
# The contents of the server are packaged in two parts, a compressed archive from http://cdimage.ubuntu.com/netboot/, and a (FAIR custom made) configuration file that is copied on top. 

netboot_tar_amd64="${FAIR_ARCHIVE_PATH}/repository/netboot-amd64.tar.gz"
netboot_tar_i386="${FAIR_ARCHIVE_PATH}/repository/netboot-i386.tar.gz"

if [ -f "$netboot_tar_i386" ] && [ -f "$netboot_tar_amd64" ]
then
	tar xvfz "$netboot_tar_amd64" --directory /var/tftp/
	tar xvfz "$netboot_tar_i386" --directory /var/tftp/
else
	echo "Missing $netboot_tar_i386 and/or $netboot_tar_amd64"
  exit 1
fi

copy_skel var/tftp/ubuntu-installer/i386/boot-screens/txt.cfg
copy_skel var/tftp/ubuntu-installer/i386/boot-screens/menu.cfg
copy_skel var/tftp/ubuntu-installer/i386/boot-screens/syslinux.cfg
# copy_skel var/tftp/ubuntu-installer/i386/boot-screens/splash.png

echo "Copying preseed configuration file"
# These files automate the Ubuntu installation process by selecting which
# packages to install, and by running our custom 'post-install' script. 
copy_skel var/www/html/preseed.cfg
copy_skel var/www/html/late_command.sh

