echo "---------------------------------------"
echo "X11vnc for connecting graphically to server"
echo "---------------------------------------"


apt-get install -y -q x11vnc
copy_skel etc/init.d/x11vnc
chmod +x /etc/init.d/x11vnc

systemctl reload-daemon
systemctl start x11vnc
